package com.micrich.examandroidmobilityapps.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.micrich.examandroidmobilityapps.Model.DataJson;
import com.micrich.examandroidmobilityapps.Model.Datos;
import com.micrich.examandroidmobilityapps.Model.TramaResponse;
import com.micrich.examandroidmobilityapps.Model.Ubicaciones;
import com.micrich.examandroidmobilityapps.Presenter.Servicios;
import com.micrich.examandroidmobilityapps.Presenter.ServiciosPresenter;
import com.micrich.examandroidmobilityapps.R;
import com.micrich.examandroidmobilityapps.Utils.Const;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Servicios.View {
    private Button btnCargar;
    private Button btnCargarMapa;
    private Button btnCargarBoton;
    private Servicios.Presenter presenter;
    private String nombreTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        btnCargarMapa = (Button) findViewById(R.id.btn_cargar_mapa);
        btnCargarMapa.setOnClickListener(this);

        btnCargar = (Button) findViewById(R.id.btn_cargar);
        btnCargar.setOnClickListener(this);
        btnCargar.setEnabled(false);

        btnCargarBoton = (Button) findViewById(R.id.btn_cargar_boton);
        btnCargarBoton.setOnClickListener(this);

        if (presenter == null) {
            presenter = new ServiciosPresenter(this);
        }

        getPermissionsWrite();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cargar_mapa:
                startActivity(new Intent(this, MapActivity.class));
                break;

            case R.id.btn_cargar:
                Toast.makeText(this, "Espera un momento", Toast.LENGTH_SHORT).show();
                presenter.coordenadas("55", "12984");
                break;

            case R.id.btn_cargar_boton:
                startActivity(new Intent(this, ProgramaticoActivity.class));
                break;
        }
    }

    @Override
    public void exitoso(TramaResponse tramaResponse) {
        if (descargaZip(tramaResponse.getValueResponse(), Const.NAME_FILE)) {
            if (descomprimeZip(Const.NAME_FILE)) {
                DataJson dataJson = cargarDatos(nombreTxt);
                List<Ubicaciones> ubicaciones = null;
                for (Datos datos: dataJson.getData()) {
                    ubicaciones = datos.getUBICACIONES();
                }

                String listUbicaciones = new Gson().toJson(ubicaciones);
                Intent intent = new Intent(this, MapActivity.class);
                intent.putExtra(Const.EXTRA_DATAJSON, listUbicaciones);
                startActivity(intent);
            }
        }
    }

    @Override
    public void setPresenter(Servicios.Presenter presenter) {
        this.presenter = presenter;
    }

    public boolean descargaZip(String url, String outputFileName) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            URL downloadUrl = new URL(url);

            File file = new File(dir, outputFileName + Const.EXT_FILE);
            URLConnection ucon = downloadUrl.openConnection();

            InputStream is = ucon.getInputStream();


            FileOutputStream fos = new FileOutputStream(file);
            byte[] data = new byte[4096];
            int count;
            while ((count = is.read(data)) != -1) {
                fos.write(data, 0, count);
            }
            fos.flush();
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean descomprimeZip(String outputFileName) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/");

        if (dir.exists()) {
            try {
                File file = new File(dir, outputFileName + Const.EXT_FILE);
                ZipInputStream zis = new ZipInputStream(new FileInputStream(file));

                ZipEntry salida;

                while (null != (salida = zis.getNextEntry())) {
                    nombreTxt = salida.getName();
                    File fileTxt = new File(dir, salida.getName());
                    FileOutputStream fos = new FileOutputStream(fileTxt);
                    int leer;
                    byte[] buffer = new byte[1024];
                    while (0 < (leer = zis.read(buffer))) {
                        fos.write(buffer, 0, leer);
                    }
                    fos.close();
                    zis.closeEntry();
                }
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
        } else {
            Log.d("TAG" ,"No se encontró el directorio..");
        }
        return false;
    }

    public DataJson cargarDatos(String nombreTxt) {
        DataJson dataJson = null;
        File dir = new File(Environment.getExternalStorageDirectory() + "/");

        if (dir.exists()) {
            try {
                File file = new File(dir, nombreTxt);
                InputStream archivo = new FileInputStream(file);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(archivo));
                StringBuilder stringBuilder = new StringBuilder();

                String linea;
                while((linea =bufferedReader.readLine())!= null){
                    stringBuilder.append(linea);
                }
                archivo.close();
                bufferedReader.close();

                Gson g = new Gson();
                dataJson = g.fromJson(stringBuilder.toString(), DataJson.class);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dataJson;
    }

    public void getPermissionsWrite (){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Const.MY_WRITE_EXTERNAL_STORAGE);
            }
        } else {
            btnCargar.setEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Const.MY_WRITE_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                btnCargar.setEnabled(true);
            } else {
                btnCargar.setEnabled(false);
            }
        }
    }
}
