package com.micrich.examandroidmobilityapps.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.micrich.examandroidmobilityapps.Presenter.Hilo;
import com.micrich.examandroidmobilityapps.Presenter.HiloPresenter;
import com.micrich.examandroidmobilityapps.R;

public class ProgramaticoActivity extends AppCompatActivity implements Hilo.View {
    private Hilo.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        layout.setLayoutParams(layoutparams);

        Button bt = new Button(this);
        bt.setText(getResources().getText(R.string.str_cargar_hilo));
        bt.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        layout.addView(bt);

        setContentView(layout);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Corriendo Hilo", Toast.LENGTH_SHORT).show();
                presenter.cargarHilo();
            }
        });

        if (presenter == null) {
            presenter = new HiloPresenter(this);
        }
    }

    @Override
    public void exitoso() {
        alertaHilo();
    }

    public void alertaHilo() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.str_hilo));
        alertDialog.setMessage(getString(R.string.str_hilo_terminado));
        alertDialog.setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void setPresenter(Hilo.Presenter presenter) {
        this.presenter = presenter;
    }
}
