package com.micrich.examandroidmobilityapps.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.micrich.examandroidmobilityapps.Model.Ubicaciones;
import com.micrich.examandroidmobilityapps.R;
import com.micrich.examandroidmobilityapps.Utils.Const;

import java.util.List;
import java.util.regex.Pattern;

public class MapActivity extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private int initialLocation = 0;
    private Location miLocalizacion;
    private EditText edtCoordenadas;
    private Button btnMarcar;
    private String longitud = "";
    private String latitud = "";
    private List<Ubicaciones> ubicaciones;
    private boolean cargaU = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        edtCoordenadas = (EditText) findViewById(R.id.edt_coordenadas);
        btnMarcar = (Button) findViewById(R.id.btn_marcar);
        btnMarcar.setOnClickListener(this);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.maps);

        if (!gpsHabilitado()) {
            showSettingsAlert();
        } else {
            mapFragment.getMapAsync(this);
        }

        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            String strUbicaciones = bundle.getString(Const.EXTRA_DATAJSON);
            ubicaciones = new Gson().fromJson(strUbicaciones, new TypeToken<List<Ubicaciones>>(){}.getType());
            if (ubicaciones != null) {
                cargaU = true;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_marcar:
                String coordenadas = edtCoordenadas.getText().toString();
                validaCoordenadas(coordenadas);
                break;
        }
    }

    private boolean validaC(String cor){
        Pattern patron = Pattern.compile("^([0-9]{2}.[0-9]+) -([0-9]{2}.[0-9]+)$");
        return patron.matcher(cor).matches();
    }

    public void validaCoordenadas(String coordenadas) {
        String[] coord = coordenadas.split(" ");
        if (coord.length == 2) {
            if (validaC(coordenadas)) {
                for (String cor: coord) {
                    if (cor.contains("-")) {
                        longitud = cor;
                    } else {
                        latitud = cor;
                    }
                }

                BitmapDescriptor marcadorPropio = BitmapDescriptorFactory.fromResource(R.drawable.loc);
                LatLng latLng = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
                mMap.addMarker(new MarkerOptions().position(latLng).title("Marker MobilityApps").icon(marcadorPropio));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            } else {
                Toast.makeText(this, "Datos incorrectos. Ejmp. 19.2969 -99.1918", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Datos incorrectos. Ejmp. 19.2969 -99.1918", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        //Initialize Google Play Services

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                iniciarDatos();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            iniciarDatos();
        }
    }

    private void iniciarDatos() {
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            if (gpsHabilitado()){
                miLocalizacion = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }

            moverCamera();

            if (cargaU) {
                marcarUbicaciones(ubicaciones);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void moverCamera() {
        if (miLocalizacion != null) {
            BitmapDescriptor marcadorPropio = BitmapDescriptorFactory.fromResource(R.drawable.loc);
            initialLocation++;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(miLocalizacion.getLatitude(),
                            miLocalizacion.getLongitude()), 13));
//            mMap.addMarker(new MarkerOptions().position(new LatLng(miLocalizacion.getLatitude(),
//                    miLocalizacion.getLongitude())).title("Marker MobilityApps").icon(marcadorPropio));
        }
    }

    public void marcarUbicaciones(List<Ubicaciones> ubicaciones) {
        BitmapDescriptor marcadorPropio = BitmapDescriptorFactory.fromResource(R.drawable.loc);
        for (Ubicaciones ubicacion: ubicaciones) {
            LatLng latLng = new LatLng(ubicacion.getFNLATITUD(), ubicacion.getFNLONGITUD());
            mMap.addMarker(new MarkerOptions().position(latLng).title("Marker MobilityApps").icon(marcadorPropio));
        }
    }

    public boolean gpsHabilitado() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.gps_no_habilitado));
        alertDialog.setMessage(getString(R.string.activar_gps));
        alertDialog.setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION){
            if (resultCode == 0){
                if (gpsHabilitado()){
                    mapFragment.getMapAsync(this);
                } else {
                    showSettingsAlert();
                }
            }
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(MapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        miLocalizacion = location;
        if (miLocalizacion != null && initialLocation == 0){
            moverCamera();
            initialLocation++;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    if (gpsHabilitado()) {
                        mapFragment.getMapAsync(this);
                    } else {
                        showSettingsAlert();
                    }
                }

            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
