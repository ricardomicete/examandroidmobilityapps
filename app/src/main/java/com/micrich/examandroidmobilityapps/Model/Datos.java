package com.micrich.examandroidmobilityapps.Model;

import java.util.List;

public class Datos {
    private List<Ubicaciones> UBICACIONES;

    public List<Ubicaciones> getUBICACIONES() {
        return UBICACIONES;
    }

    public void setUBICACIONES(List<Ubicaciones> UBICACIONES) {
        this.UBICACIONES = UBICACIONES;
    }

    @Override
    public String toString() {
        return "Datos{" +
                "UBICACIONES=" + UBICACIONES +
                '}';
    }
}
