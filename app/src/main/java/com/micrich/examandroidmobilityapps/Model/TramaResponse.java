package com.micrich.examandroidmobilityapps.Model;

public class TramaResponse {
    private int code;
    private String hasError;
    private String valueResponse;

    public TramaResponse(int code, String hasError, String valueResponse) {
        this.code = code;
        this.hasError = hasError;
        this.valueResponse = valueResponse;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String isHasError() {
        return hasError;
    }

    public void setHasError(String hasError) {
        this.hasError = hasError;
    }

    public String getValueResponse() {
        return valueResponse;
    }

    public void setValueResponse(String valueResponse) {
        this.valueResponse = valueResponse;
    }

    @Override
    public String toString() {
        return "TramaResponse{" +
                "code=" + code +
                ", hasError='" + hasError + '\'' +
                ", valueResponse='" + valueResponse + '\'' +
                '}';
    }
}
