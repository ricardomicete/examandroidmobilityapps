package com.micrich.examandroidmobilityapps.Model;

import java.util.List;

public class DataJson {
    private List<Datos> data;
    private boolean hasError;

    public DataJson(List<Datos> data, boolean hasError) {
        this.data = data;
        this.hasError = hasError;
    }

    public List<Datos> getData() {
        return data;
    }

    public void setData(List<Datos> data) {
        this.data = data;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    @Override
    public String toString() {
        return "DataJson{" +
                "data=" + data +
                ", hasError=" + hasError +
                '}';
    }
}
