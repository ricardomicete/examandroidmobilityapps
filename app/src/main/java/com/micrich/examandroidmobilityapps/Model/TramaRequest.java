package com.micrich.examandroidmobilityapps.Model;

public class TramaRequest {
    private String serviceId;
    private String userId;

    public TramaRequest(String serviceId, String userId) {
        this.serviceId = serviceId;
        this.userId = userId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TramaRequest{" +
                "serviceId='" + serviceId + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
