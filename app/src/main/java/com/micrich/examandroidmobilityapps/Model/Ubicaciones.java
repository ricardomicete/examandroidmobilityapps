package com.micrich.examandroidmobilityapps.Model;

public class Ubicaciones {
    private double FNLATITUD;
    private double FNLONGITUD;

    public Ubicaciones(double FNLATITUD, double FNLONGITUD) {
        this.FNLATITUD = FNLATITUD;
        this.FNLONGITUD = FNLONGITUD;
    }

    public double getFNLATITUD() {
        return FNLATITUD;
    }

    public void setFNLATITUD(double FNLATITUD) {
        this.FNLATITUD = FNLATITUD;
    }

    public double getFNLONGITUD() {
        return FNLONGITUD;
    }

    public void setFNLONGITUD(double FNLONGITUD) {
        this.FNLONGITUD = FNLONGITUD;
    }

    @Override
    public String toString() {
        return "Ubicaciones{" +
                "FNLATITUD=" + FNLATITUD +
                ", FNLONGITUD=" + FNLONGITUD +
                '}';
    }
}
