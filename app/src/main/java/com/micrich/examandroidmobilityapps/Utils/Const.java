package com.micrich.examandroidmobilityapps.Utils;

public class Const {
    public static final int CODIGO_200 = 200;
    public static final int EXITOSO = 1;
    public static final String NAME_FILE = "coordenadasZip";
    public static final String EXT_FILE = ".zip";
    public static final String EXT_FILE_TXT = ".txt";
    public static final int MY_WRITE_EXTERNAL_STORAGE = 101;
    public static final int MY_READ_EXTERNAL_STORAGE = 102;
    public static final String EXTRA_DATAJSON = "EXTRA_DATAJSON";
    public static final int DIEZ_SEGUNDOS = 10000;
}
