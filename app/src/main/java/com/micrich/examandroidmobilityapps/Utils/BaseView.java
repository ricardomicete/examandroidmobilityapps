package com.micrich.examandroidmobilityapps.Utils;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
