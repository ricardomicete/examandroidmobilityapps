package com.micrich.examandroidmobilityapps.Ws;

import retrofit2.Retrofit;

public class Service {
    private static Service self;
    private Retrofit retrofit;

    private Service() {
        retrofit = RetrofitWs.getInstance().apiService();
    }

    public static Service getInstance() {
        if (self == null) {
            self = new Service();
        }
        return self;
    }

    public RegisterInterface getPublicApi() {
        return retrofit.create(RegisterInterface.class);
    }
}
