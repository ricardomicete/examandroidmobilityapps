package com.micrich.examandroidmobilityapps.Ws;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitWs {
    private static final String URL = "http://74.205.41.248:8081/";
    private static RetrofitWs self;

    public static RetrofitWs getInstance() {
        if (self == null) {
            self = new RetrofitWs();
        }
        return self;
    }

    private Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create());

    Retrofit apiService(){
        return builder.baseUrl(URL).build();
    }
}
