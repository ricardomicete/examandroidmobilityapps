package com.micrich.examandroidmobilityapps.Ws;

import com.micrich.examandroidmobilityapps.Model.TramaRequest;
import com.micrich.examandroidmobilityapps.Model.TramaResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RegisterInterface {

    @POST("pruebawebservice/api/mob_sp_GetArchivo")
    Call<List<TramaResponse>> coordenadas(@Body TramaRequest body);
}
