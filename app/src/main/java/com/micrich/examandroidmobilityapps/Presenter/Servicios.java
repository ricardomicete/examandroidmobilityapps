package com.micrich.examandroidmobilityapps.Presenter;

import com.micrich.examandroidmobilityapps.Model.TramaResponse;
import com.micrich.examandroidmobilityapps.Utils.BaseView;

public class Servicios {
    public interface Presenter {
        void coordenadas(String serviceId, String userId);
    }

    public interface View extends BaseView<Presenter> {
        void exitoso(TramaResponse tramaResponse);
    }
}
