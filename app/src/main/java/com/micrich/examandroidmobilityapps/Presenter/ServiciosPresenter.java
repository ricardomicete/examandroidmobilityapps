package com.micrich.examandroidmobilityapps.Presenter;

import android.util.Log;

import com.micrich.examandroidmobilityapps.Model.TramaRequest;
import com.micrich.examandroidmobilityapps.Model.TramaResponse;
import com.micrich.examandroidmobilityapps.Utils.Const;
import com.micrich.examandroidmobilityapps.Ws.Service;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiciosPresenter extends BasePresenter<Servicios.View> implements Servicios.Presenter {
    public ServiciosPresenter(Servicios.View view) {
        super(view);
    }

    @Override
    public void coordenadas(String serviceId, String userId) {
        Call<List<TramaResponse>> tramaResponseCall =
                Service.getInstance().getPublicApi().coordenadas(new TramaRequest(serviceId, userId));

        tramaResponseCall.enqueue(new Callback<List<TramaResponse>>() {
            @Override
            public void onResponse(Call<List<TramaResponse>> call, Response<List<TramaResponse>> response) {
                int statusCode = response.code();
                List<TramaResponse> tramaResponse = response.body();
                if (tramaResponse != null) {
                    if (statusCode == Const.CODIGO_200 && tramaResponse.get(0).getCode() == Const.EXITOSO) {
                        mView.exitoso(tramaResponse.get(0));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TramaResponse>> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage() + t);
            }
        });
    }
}
