package com.micrich.examandroidmobilityapps.Presenter;

import com.micrich.examandroidmobilityapps.Utils.BaseView;

public class BasePresenter<V extends BaseView>{
    protected V mView;

    public BasePresenter(V view) {
        mView = view;
        view.setPresenter(this);
    }
}
