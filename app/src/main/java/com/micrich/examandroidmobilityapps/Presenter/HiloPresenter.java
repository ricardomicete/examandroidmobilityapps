package com.micrich.examandroidmobilityapps.Presenter;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.micrich.examandroidmobilityapps.Utils.Const;

public class HiloPresenter extends BasePresenter<Hilo.View> implements Hilo.Presenter {

    public HiloPresenter(Hilo.View view) {
        super(view);
    }

    @Override
    public void cargarHilo() {
//        Thread thread = new Thread() {
//            public void run() {
//                try {
//                    Thread.sleep(Const.DIEZ_SEGUNDOS);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                mView.exitoso();
//            }
//        };
//        thread.start();

        CargaHilo cargarHilo = new CargaHilo();
        cargarHilo.execute();

    }

    @SuppressLint("StaticFieldLeak")
    public class CargaHilo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(Const.DIEZ_SEGUNDOS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mView.exitoso();
        }

    }
}
