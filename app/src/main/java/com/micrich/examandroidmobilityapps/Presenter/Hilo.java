package com.micrich.examandroidmobilityapps.Presenter;

import com.micrich.examandroidmobilityapps.Utils.BaseView;

public class Hilo {
    public interface Presenter {
        void cargarHilo();
    }

    public interface View extends BaseView<Presenter> {
        void exitoso();
    }
}
